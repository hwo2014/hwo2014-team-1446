from numpy import array,linalg,vstack,empty,append

class LinearAccelerationModel:
	def __init__(self, log):
		self.log = log
		
		self.w0 = None #constant
		self.w1 = None #throttle
		self.w2 = None #speed (and not squared speed)

		self.x = empty([0, 3])
		self.y = array([ ])

		self.skip = None
		self.start = None

		self.initialised = False

	def reset(self):
		self.skip = None
		self.start = None

	def findSkip(self):
		#Assumes throttle would be non-zero on spawn
		try:
			if not self.start:
				if self.log.history[-1]["variables"].throttle > 0:
					self.start = len(self.log.history)

					print "Start:", self.start
			else:
				if self.log.history[-1]["variables"].acceleration > 0:
					delay = len(self.log.history)
					self.skip = delay - self.start

					print "Delay:", delay
					print "Skip:", self.skip

					if not self.initialised:
						#Zeroth element
						row = array([1, 0, 0])
						self.x = vstack((self.x, row))
						self.y = append(self.y, 0)

						self.initialised = True
		except:
			pass


	def get_current_acceleration(throttle, velocity, turboFactor=1):
		return self.w0 + self.w1 * throttle * turbofactor - self.w2 * velocity

	def get_current_throttle(vDesired, vCurrent, turboFactor=1):
		a = vDesired - vCurrent
		#Note that this could be an impossible value (<0 or >1) as well
		return ((a - self.w0 + self.w2*vCurrent) / self.w1 * turboFactor)

	def update(self):
		# set_trace()
		if not self.skip:
			self.findSkip()
		else:
			throttle = self.log.history[-(self.skip+1)]["variables"].throttle
			turboFactor = self.log.history[-(self.skip)]["variables"].prevTurboFactor
			# turboFactor = 1
			speed = self.log.history[-(self.skip+1)]["variables"].speed
			row = array([1, (throttle*turboFactor), speed]) # Do not square!
			
			# print row, self.log.history[-1]["variables"].acceleration

			self.x = vstack((self.x, row))

			acceleration = self.log.history[-1]["variables"].acceleration
			self.y = append(self.y, acceleration)

			self.w0, self.w1, self.w2 = linalg.lstsq(self.x, self.y)[0]