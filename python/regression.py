from numpy import arange,array,ones,linalg,vstack,empty,append
import csv

x = [[], [], []]
y = []

x_n = empty([0, 3])
y_n = array([ ])

with open('log.csv', 'rU') as fp:
    a = csv.reader(fp, delimiter=',', quotechar='|')
    next(a, None)
    for row in a:
        # print row

        x[0].append(float(row[0]))
        x[1].append(float(row[1]))

        temp = array([1, float(row[0]), float(row[1])])
        x_n = vstack((x_n, temp))
        
        # insert(x_n, [ float(row[0], float(row[1]) ] )

        # x[2].append(float(row[2]))
        y.append(float(row[2]))
        y_n = append(y_n, float(row[2]))
        # insert(y_n, float(row[1]))

from numpy import set_printoptions,column_stack
set_printoptions(threshold='nan')
print column_stack((x_n, y_n))

w = linalg.lstsq(x_n,y_n)[0] # obtaining the parameters
print w

print "Changin weights"
w[0] = 1.61518727416e-13 
w[1] =   0.203549474076
w[2] = -0.0196289963307

  

# 0.00300774084141  

# A = array( [ ones( len(x[0]) ), x[0], x[1] ] )
# w = linalg.lstsq(A.T,y)[0] # obtaining the parameters
# print w

sum_pred = 0
count = 0

with open('log.csv', 'rU') as fp:
    a = csv.reader(fp, delimiter=',', quotechar='|')
    next(a, None)
    for row in a:
        throttle = float(row[0])
        velsq = float(row[1])
        accel = float(row[2])


        pred = w[0] + w[1]*throttle + w[2] * velsq

        diff = abs(accel - pred)
        sum_pred += diff
        count += 1

        print pred, accel, diff

print "Mean error:", sum_pred / count
print w[0], w[1], w[2]
