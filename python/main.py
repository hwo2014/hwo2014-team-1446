import json
import socket
import sys
from strategy import Strategy
import csv
#import numpy


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.game_start = False
        self.strategy = Strategy()

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def msg_with_gameTick(self, msg_type, data, gameTick):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": gameTick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def create_race(self, trackName):
        msg = json.dumps({"msgType": "createRace", "data": {
                "botId": {
                    "name": self.name,
                    "key": self.key 
                },
                "trackName": trackName,
                "password": "f14b17",
                "carCount": 1
                }})

        self.send(msg)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        #self.join()
        self.create_race("keimola")
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        # self.strategy.setColor(data['color'], data['name'])
        self.strategy.set_car_id({"name":data['name'], "color": data['color']})
        self.ping()

    def on_game_init(self, data):
        self.strategy.set_init_data(data)

    def on_game_start(self, data):
        self.game_start = True
        print("Race started")
        self.ping()

    def on_car_positions_init(self, data):
        self.strategy.set_init_car_position(data)

    def on_car_positions_update(self, data, gameTick): #main game loop
        self.strategy.update_car_positions(data, gameTick)
        msg_type, data = self.strategy.get_action(gameTick)

        if (not msg_type) or (not data):
            self.ping()
        else:
            self.msg_with_gameTick(msg_type, data, gameTick)

    def on_crash(self, data):
        print("Someone crashed")
        self.strategy.notify_crash(data)
        self.ping()

    def on_spawn(self, data):
        self.strategy.notify_spawn(data)
        self.ping()

    def on_lap_finished(self, data):
        self.strategy.update_lap_count(data)

    def on_turbo_available(self, data):
        self.strategy.set_turbo_available(data)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print("\n")
        
        print json.dumps(self.strategy.initData)

        # skip_start = None
        # skip_delay = None

        # for i,item in enumerate(self.strategy.log.history):
        #     if item["variables"].throttle > 0:
        #         skip_start = i
        #         break

        # for i,item in enumerate(self.strategy.log.history):
        #     if item["variables"].acceleration > 0:
        #         skip_delay = i
        #         break

        # print "skip_delay", skip_delay
        # print "skip_start", skip_start

        # skip = skip_delay - skip_start

        import copy

        history = copy.copy(self.strategy.log.history)

        with open('./log-all.csv','wb') as f:
            a = csv.writer(f, delimiter=',', quotechar='|')
            a.writerow(["angle","speed","distance","throttle","acceleration","angularVelocity","angularAcceleration","trackRadius"])
            for item in history:
                item["variables"].gameTick = item["gameTick"]
                item["variables"].crashed = item["crashed"]
                print item["variables"]

                a.writerow([ item["variables"].angle,
                            # item["variables"].lap,
                            item["variables"].speed,
                            item["variables"].distance,
                            item["variables"].throttle,
                            item["variables"].acceleration,
                            item["variables"].angularVelocity,
                            item["variables"].angularAcceleration,
                            item["variables"].trackRadius])
        
        # from numpy import set_printoptions,column_stack
        # set_printoptions(threshold='nan')

        # print column_stack((self.strategy.linearAcceleration.x, 
        #                 self.strategy.linearAcceleration.y))
        
        # print self.strategy.linearAcceleration.w0, self.strategy.linearAcceleration.w1, self.strategy.linearAcceleration.w2

        # # with open('./log.csv','wb') as f:
        # #     a = csv.writer(f, delimiter=',', quotechar='|')
        # #     a.writerow(["t","sq","a"])
        # #     for i,item in enumerate(self.strategy.log.history):
        # #         if (item["crashed"] == False):
        # #                 try:
        # #                     a.writerow([(item["variables"].throttle*self.strategy.log.history[i+1]["variables"].prevTurboFactor), 
        # #                         item["variables"].speed, 
        # #                         self.strategy.log.history[i+skip]["variables"].acceleration])
        # #                 except:
        # #                     #we've gone over max index
        # #                     pass

        # skip = 0
        # with open('./angle.csv','wb') as f:
        #     a = csv.writer(f, delimiter=',', quotechar='|')
        #     a.writerow(["vsq-by-rsq", "v", "vsq", "omega", "omega-sq", "accel", "radius", "alpha"])
        #     for i,item in enumerate(self.strategy.log.history):
        #         # from pudb import set_trace
        #         # set_trace()
        #         try:
        #             row = [(item["variables"].speed**2/item["variables"].trackRadius**2),
        #                 item["variables"].speed,
        #                 item["variables"].speed**2,  
        #                 item["variables"].angularVelocity,  
        #                 item["variables"].angularVelocity**2,
        #                 item["variables"].acceleration,
        #                 item["variables"].radius,
        #                 self.strategy.log.history[i+1]["variables"].angularAcceleration]

        #             print row
        #             a.writerow()

        #             # if (item["variables"].speed**2/item["variables"].trackRadius**2 == 0) and (self.strategy.log.history[i+1]["variables"].angularAcceleration != 0):
        #             #     from pudb import set_trace
        #             #     set_trace()
        #         except:
        #             #we've gone over max index
        #             pass

        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            # 'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            # 'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_join,
            'turboAvailable': self.on_turbo_available,
            'lapFinished': self.on_lap_finished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()

        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                    msg_map[msg_type](data)
            elif msg_type == "carPositions":
                if self.game_start:
                    if msg.has_key('gameTick'):
                        self.on_car_positions_update(msg['data'], msg['gameTick'])
                    else:
                        #race has ended
                        pass
                else:
                    self.on_car_positions_init(msg['data'])
            elif msg_type == "turboStart":
                self.strategy.start_turbo(msg['data'], msg['gameTick'])
            elif msg_type == "turboEnd":
                self.strategy.end_turbo(msg['data'], msg['gameTick'])
            else:
                print("Got unhandled {0}".format(msg_type))
                print json.dumps(data)
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
