from hwoUtils import HWOUtils
import sys
import copy
import random
from linearAccelerationModel import LinearAccelerationModel

class CarVariables(object):
    def __init__(self):
        self.angle = None
        # self.lap = None
        self.piecePosition = None
        
        # self.prevIndex = 0
        # self.prevInPieceDistance = 0.0
        # self.prevDistance = 0.0

        self.speed = 0
        self.distance = 0
        self.throttle = 0
        self.acceleration = 0
        self.prevTurboFactor = 1

        self.angularVelocity = 0.0
        self.angularAcceleration = 0.0
        self.trackRadius = 0.0

    def __str__(self):
        return str(self.__dict__)

class Car(object):
    def __init__(self, log):
        self.id = {}
        self.dimensions = None
        self.variables = CarVariables()
        self.trackInfo = None
        self.gameTick = -1;

        self.log = log

        self.initialised = False


    def update_properties(self, crashed):
        #update speed, distance and acceleration
        if(self.trackInfo):
            self.update_distance_and_speed(crashed)
        else:
            print "TRACKINFO NOT DEFINED!"
        
    def update_distance_and_speed(self, crashed=False):
        """
        self.trackInfo[track piece#][current lane #]
        #['type': 0-straight,1-switch,2-bend;3-bend with a switch
        #   'distance': distance while staying in the current lane
        #   'switch-distance': list of dictionary with total number of options; each option has 'toLaneIndex','distance']
        """
        currInPieceDistance = self.variables.piecePosition["inPieceDistance"]
        currPieceIndex = self.variables.piecePosition["pieceIndex"]
        currFromLane = self.variables.piecePosition["lane"]["startLaneIndex"]
        currToLane = self.variables.piecePosition["lane"]["endLaneIndex"]

        if self.log.lastTick > -1:
            prevInPieceDistance = self.log.history[-1]["variables"].piecePosition["inPieceDistance"]
            prevPieceIndex = self.log.history[-1]["variables"].piecePosition["pieceIndex"]
            prevFromLane = self.log.history[-1]["variables"].piecePosition["lane"]["startLaneIndex"]
            prevToLane = self.log.history[-1]["variables"].piecePosition["lane"]["endLaneIndex"]
            prevSpeed = self.log.history[-1]["variables"].speed
            prevTick = self.log.history[-1]["gameTick"]
            prevAngle = self.log.history[-1]["variables"].angle
            prevAngularVelocity = self.log.history[-1]["variables"].angularVelocity
        else:
            prevPieceIndex = 0
            prevInPieceDistance = 0.0
            prevFromLane = currFromLane
            prevToLane = currFromLane
            prevSpeed = 0
            prevTick = -1
            prevAngle = 0.0
            prevAngularVelocity = 0.0

        #Update current distance
        gain = 0.0
        dist = 0.0

        if (prevPieceIndex == currPieceIndex):
            #Still in the current piece
            gain = currInPieceDistance - prevInPieceDistance
        else:
            #You are one piece ahead from previous tick
            dist = 0.0
            if (prevFromLane != prevToLane):
                dist = self.trackInfo[prevPieceIndex][prevToLane][str(prevFromLane)]
            else:
                dist = self.trackInfo[prevPieceIndex][prevToLane]["distance"]
            gain = currInPieceDistance + (dist - prevInPieceDistance)

        #Update distance
        self.variables.distance += gain

        #Update speed and acceleration
        deltaTick = abs(self.gameTick - prevTick)
        self.variables.speed = gain / deltaTick
        self.variables.acceleration = (self.variables.speed - prevSpeed) / deltaTick

        #Update angular properties
        if(crashed):
            self.variables.angularVelocity = 0.0
            self.variables.angularAcceleration = 0.0
        else:
            currAngle = self.variables.angle
            self.variables.angularVelocity = (currAngle - prevAngle)/deltaTick
            self.variables.angularAcceleration = (self.variables.angularVelocity - prevAngularVelocity) / deltaTick

        #Update track angle
        if (self.trackInfo[currPieceIndex][currFromLane]["type"] in [2, 3]):
            if (currFromLane != currToLane):
                self.variables.trackRadius = self.trackInfo[currPieceIndex][currFromLane]["radius"+str(currToLane)]
            else:
                self.variables.trackRadius = self.trackInfo[currPieceIndex][currFromLane]["radius"]
        else:
            self.variables.trackRadius = float("inf")

        # if(self.variables.acceleration < 0):
            # from pudb import set_trace
            # set_trace()

        # self.variables.prevIndex = currPieceIndex
        # self.variables.prevInPieceDistance = currInPieceDistance


    def set_throttle(self, throttle):
        #do any changes specific to the car
        self.variables.throttle = throttle

    def set_turboFactor(self, turboFactor):
        self.variables.prevTurboFactor = turboFactor

    def set_position(self, pos, gameTick, crashed=False):
        self.gameTick = gameTick

        self.variables.angle = pos["angle"]
        self.variables.piecePosition = pos["piecePosition"]

        self.update_properties(crashed)
        self.initialised = True

    def set_trackinfo(self, trackInfo):
        self.trackInfo = trackInfo
        # print "SET TRACKINFO", self.trackInfo

    def reset(self):
        self.variables.speed = 0
        self.variables.throttle = 0
        self.variables.prevTurboFactor = 1
        
        self.variables.angularVelocity = 0
        self.angle = 0


class CarLogger(object):
    def __init__(self):
        self.lastTick = -1
        self.history = []

    def add_position(self, variables, gameTick, crashed=False):
        self.lastTick = gameTick
        self.history.append({"gameTick": gameTick, "variables": copy.copy(variables), "crashed": crashed})

class Strategy(object):
    def __init__(self):
        self.initData = None
        self.log = CarLogger()
        self.car = Car(self.log)
        self.linearAcceleration = LinearAccelerationModel(self.log)
        self.turbo = None
        self.lapCount = 0
        self.ask_for_switch_index = None
        self.slow_down = -50

        """
        self.trackInfo[track piece#][current lane #]
        #['type': 0-straight,1-switch,2-bend;
        #   'distance': distance while staying in the current lane
        #   'switch-distance': list of dictionary with total number of options; each option has 'toLaneIndex','distance']
        """
        self.trackInfo = None
        self.crashed = False

    def set_car_id(self, id):
        self.car.id = id

    def set_init_data(self, data):
        self.initData = data
        self.infer_track_info()

        self.infer_track_info()

        cars = data["race"]["cars"]
        
        for c in cars:
            if(c["id"] == self.car.id):
                self.car.dimensions = c["dimensions"]
                break;

    def infer_track_info(self):
        nLanes = len(self.initData["race"]["track"]["lanes"])
        distFromCenter = [0.0]*nLanes
        nTrackPieces = len(self.initData["race"]["track"]["pieces"])
        self.trackInfo = []
        for i in xrange(0,nTrackPieces):
            self.trackInfo .append([])
            for j in xrange(0,nLanes):
                self.trackInfo [i].append(dict())

        #Computes and stores distance from the center of the lane
        for lane in self.initData["race"]["track"]["lanes"]:
            laneID = lane["index"]
            distFromCenter[laneID] = lane["distanceFromCenter"]

        #Builds data-structure to store piece-wise track distance
        for i in range(0,nTrackPieces):
            piece = self.initData["race"]["track"]["pieces"][i]
            #Current lane you are on
            for j in range(0,nLanes):
                if (("switch" in piece.keys()) and ("angle" not in piece.keys())):
                    #You switch
                    if j == 0 or j == (nLanes-1):
                        #Corner lanes
                        d = 0.0
                        toLaneID = 0

                        if j == 0:
                            #Left corner -> adjacent

                            #Don't make a switch
                            self.trackInfo[i][j] = {"type": 1, "distance": piece["length"]}
                            d0 = distFromCenter[j];
                            d1 = distFromCenter[j+1];
                            toLaneID = j+1

                            if (((d0 >= 0.0) and (d1 >= 0.0)) or ((d0 < 0.0) and (d1 < 0.0))):
                                d = [abs(d0),  abs(d1)]
                                d = max(d)-min(d)
                            else:
                                d = abs(d0) + abs(d1)

                        else:
                            #Right corner -> adjacent
                            self.trackInfo[i][j] = {"type": 1, "distance": piece["length"]}
                            d0 = distFromCenter[j]
                            d1 = distFromCenter[j-1]
                            toLaneID = j-1

                            if (((d0 >= 0.0) and (d1 >= 0.0)) or ((d0 < 0.0) and (d1 < 0.0))):
                                d = [abs(d0),  abs(d1)]
                                d = max(d)-min(d)
                            else:
                                d = abs(d0) + abs(d1)
                        #Make the switch
                        self.trackInfo[i][j].update({str(toLaneID) : HWOUtils.get_hypotenuse(piece["length"], d)})
                    else:
                        #Somewhere in between

                        #Don't make the switch
                        self.trackInfo[i][j] = {"type": 1, "distance": piece["length"]}
                        toLaneID = 0
                        d = 0.0

                        d0 = distFromCenter[j]
                        d1 = distFromCenter[j-1]
                        d2 = distFromCenter[j+1]

                        #option1
                        toLaneID = j-1
                        if (((d0 >= 0.0) and (d1 >= 0.0)) or ((d0 < 0.0) and (d1 < 0.0))):
                            d = [abs(d0),  abs(d1)]
                            d = max(d)-min(d)
                        else:
                            d = abs(d0) + abs(d1)
                        #Make the switch type 1
                        self.trackInfo[i][j].update({str(toLaneID) : HWOUtils.get_hypotenuse(piece["length"], d)})

                        #option2
                        toLaneID = j+1
                        if (((d0 >= 0.0) and (d2 >= 0.0)) or ((d0 < 0.0) and (d2 < 0.0))):
                            d = [abs(d0),  abs(d2)]
                            d = max(d)-min(d)
                        else:
                            d = abs(d0) + abs(d2)
                        #Make the switch type 2
                        self.trackInfo[i][j].update({str(toLaneID) : HWOUtils.get_hypotenuse(piece["length"], d)})
                elif (("angle" in piece.keys()) and ("switch" not in piece.keys())):
                    #You are in a bend
                    r = piece["radius"]
                    if piece["angle"] >= 0.0:
                        r = r - distFromCenter[j]
                    else:
                        r = r + distFromCenter[j]
                    
                    theta = abs(piece["angle"])
                    self.trackInfo[i][j] = {"type": 2, "distance": HWOUtils.get_arc_length(HWOUtils.get_circumference(r),theta)}
                    self.trackInfo[i][j].update({"radius": r})
                elif (("angle" in piece.keys()) and ("switch" in piece.keys())):
                    #You are in a bend with a switch
                    #Didn't switch
                    r = piece["radius"]

                    if piece["angle"] >= 0.0:
                        r = r - distFromCenter[j]
                    else:
                        r = r + distFromCenter[j]
                    
                    theta = abs(piece["angle"])
                    self.trackInfo[i][j] = {"type": 3, "distance":HWOUtils.get_arc_length(HWOUtils.get_circumference(r),theta)}
                    self.trackInfo[i][j].update({"radius": r})

                    #Switched
                    if j == 0 or j == (nLanes-1):
                        #Corner lanes
                        d = 0.0
                        toLaneID = 0
                        r = piece["radius"]
                        if j == 0:
                            #Left corner -> adjacent
                            d0 = distFromCenter[j];
                            d1 = distFromCenter[j+1];
                            toLaneID = j+1
                            d = (d0+d1)/2

                            if piece["angle"] >= 0.0:
                                r = r - d
                            else:
                                r = r + d
                        else:
                            #Right corner -> adjacent
                            d0 = distFromCenter[j]
                            d1 = distFromCenter[j-1]
                            toLaneID = j-1
                            d = (d0+d1)/2
                            
                            if piece["angle"] >= 0.0:
                                r = r - d
                            else:
                                r = r + d
                        self.trackInfo[i][j].update({str(toLaneID):HWOUtils.get_arc_length(HWOUtils.get_circumference(r),theta)})
                        self.trackInfo[i][j].update({"radius"+str(toLaneID): r})
                    else:
                        #Somewhere in between
                        d = 0.0
                        toLaneID = 0

                        d0 = distFromCenter[j]
                        d1 = distFromCenter[j-1]
                        d2 = distFromCenter[j+1]

                        #option1
                        r = piece["radius"]
                        toLaneID = j-1
                        d = (d0+d1)/2

                        if piece["angle"] >= 0.0:
                            r = r - d
                        else:
                            r = r + d
                        self.trackInfo[i][j].update({str(toLaneID):HWOUtils.get_arc_length(HWOUtils.get_circumference(r),theta)})
                        self.trackInfo[i][j].update({"radius"+str(toLaneID): r})
                        #option2
                        r = piece["radius"]
                        toLaneID = j+1
                        d = (d0+d2)/2

                        if piece["angle"] >= 0.0:
                            r = r - d
                        else:
                            r = r + d
                        self.trackInfo[i][j].update({str(toLaneID):HWOUtils.get_arc_length(HWOUtils.get_circumference(r),theta)})
                        self.trackInfo[i][j].update({"radius"+str(toLaneID): r})
                else:
                    #Straight stretch
                    self.trackInfo[i][j] = {"type": 0, "distance": piece["length"]}

        # print "TRACKINFO:", self.trackInfo
        self.car.set_trackinfo(self.trackInfo)

    def set_init_car_position(self, data):
        self.update_car_positions(data, 0)

    def update_lap_count(self, data):
        if(data["car"] == self.car.id):
            self.lapCount += 1

    def update_car_positions(self, data, gameTick):
        for cars in data:
            if(cars["id"] == self.car.id):
                if self.car.initialised:
                    #Add to log
                    self.log.add_position(self.car.variables, self.car.gameTick, self.crashed)

                self.car.set_position(cars, gameTick, self.crashed)

                break;

        if not self.crashed:
            self.linearAcceleration.update()

    def notify_crash(self, data):
        if(data == self.car.id):
            self.log.add_position(self.car.variables, self.car.gameTick, crashed=True)

            self.crashed = True
            self.car.initialised = False
            self.car.reset()
            self.turbo = None # Turbo is gone :(
            self.linearAcceleration.reset()

    def notify_spawn(self, data):
        if(data == self.car.id):
            self.crashed = False

    # Server has ended turbo
    def end_turbo(self, data, gameTick):
        #TODO: Manage turbo for other cars
        if(data == self.car.id):
            self.turbo = None
            print "Server ended turbo at " + str(gameTick)
            self.car.set_turboFactor(1)
            self.car.set_throttle(0.1)
            self.set_slow_down(gameTick)
        else:
            print "Someone else's turbo ended"

    # Setting protected period for breaking
    def set_slow_down(self, gameTick):
        print "slowing down"
        self.slow_down = gameTick

    # Actually apply, server has started
    def start_turbo(self, data, gameTick):
        #TODO: Manage turbo for other cars
        if(data == self.car.id):
            print "Server started turbo at " + str(gameTick)
            self.car.set_turboFactor(self.turbo["turboFactor"])
            self.turbo = None
        else:
            print "Someone else's turbo started"

    # Measure straight distance
    def measure_straight_distance(self):
        nPieces = len(self.trackInfo)
        currentIndex = self.car.variables.piecePosition["pieceIndex"]
        currLaneIndex = self.car.variables.piecePosition["lane"]["endLaneIndex"]
        distance = 0.0
        j = 0
        while self.trackInfo[(currentIndex + j) % nPieces][currLaneIndex]['type'] in [0,1]:
            distance = distance + self.trackInfo[(currentIndex + j) % nPieces][currLaneIndex]['distance']
            j = j + 1
        return distance


    # Ask server for turbo
    def ask_turbo(self, gameTick):
        print "Turbo " + str(self.turbo["turboFactor"]) + \
                " asked at: " + str(gameTick) + \
                " for " + str(self.turbo["turboDurationTicks"]) + " ticks"
        return "turbo", "Pow pow pow!"

    # Turbo is available
    def set_turbo_available(self, data):
        print "Got turbo!"
        self.turbo = data

    def apply_throttle(self, gameTick):
        throttle = 0.6
        self.car.set_throttle(throttle)

        return "throttle", throttle

    def switch_lane(self):
        nPieces = len(self.trackInfo)
        currentIndex = self.car.variables.piecePosition["pieceIndex"]
        currLaneIndex = self.car.variables.piecePosition["lane"]["endLaneIndex"]
        shortest = currLaneIndex
        anotherLaneIndices = []
        for lane in self.initData["race"]["track"]["lanes"]:
            if lane["index"] != currLaneIndex:
                if lane["index"] > currLaneIndex - 2 and lane["index"] < currLaneIndex + 2:
                    anotherLaneIndices.append(lane["index"])
        stay = self.trackInfo[currentIndex + 1][currLaneIndex]['distance']
        switch = {}
        for index in anotherLaneIndices:
            switch[index] = self.trackInfo[currentIndex + 1][currLaneIndex][str(index)]
        j = 2
        while not self.trackInfo[(currentIndex + j) % nPieces][currLaneIndex]['type'] in [1,3]:
            stay = stay + self.trackInfo[(currentIndex + j) % nPieces][currLaneIndex]['distance']
            for index in anotherLaneIndices:
                switch[index] = switch[index] + self.trackInfo[(currentIndex + j) % nPieces][index]['distance']
            j = j + 1
        switch[currLaneIndex] = stay
        minimum = 9999999999999999999999
        for i in range(currLaneIndex-1,currLaneIndex+2):
            if i in switch:
                if switch[i] < minimum:
                    shortest = i
                    minimum = switch[i]
        return shortest

    def make_switch(self, desiredLaneIndex):
        currLaneIndex = self.car.variables.piecePosition["lane"]["endLaneIndex"]
        currLaneDistFromCenter = None
        desiredLaneDistFromCenter = None
        for lane in self.initData["race"]["track"]["lanes"]:
            if lane['index'] == currLaneIndex:
                currLaneDistFromCenter = lane['distanceFromCenter']
        for lane in self.initData["race"]["track"]["lanes"]:
            if lane['index'] == desiredLaneIndex:
                desiredLaneDistFromCenter = lane['distanceFromCenter']
        if currLaneDistFromCenter < desiredLaneDistFromCenter:
            return "Right"
            print "Right"
        else:
            return "Left"
            print "Right"

    def set_ask_for_switch_index(self, value):
        self.ask_for_switch_index = value

    def get_action(self, gameTick):
        currPieceIndex = self.car.variables.piecePosition["pieceIndex"]
        currLaneIndex = self.car.variables.piecePosition["lane"]["endLaneIndex"]
        nPieces = len(self.initData["race"]["track"]["pieces"])
        if "switch" in self.initData["race"]["track"]["pieces"][(currPieceIndex + 1) % nPieces]:
            if self.ask_for_switch_index != currPieceIndex:
                if self.switch_lane() != currLaneIndex and gameTick > 7:
                    self.set_ask_for_switch_index(currPieceIndex)
                    return "switchLane", self.make_switch(self.switch_lane())
        #if (self.lapCount > 0) and (self.turbo != None): #and self.measure_straight_distance() > 300.0:
        #    return self.ask_turbo(gameTick)

        return self.apply_throttle(gameTick)
