import math

class HWOUtils(object):
	@staticmethod
	def get_hypotenuse(x, y):
		#x- first side, y- other side
		return math.sqrt(math.pow(x,2)+math.pow(y,2))

	@staticmethod
	def get_circumference(r):
		#r-radius
		return 2*math.pi*r

	@staticmethod
	def get_arc_length(c,theta):
		#c-circumference
		#theta-elapsed angle in degrees
		return (c*(theta/360))

	@staticmethod
	def get_angle_from_arc_length(r,d):
		#d- arc length
		#r- radius
		cir = get_circumference(r)
		f = 0
		if cir != 0:
			f = 360/cir
		return d*f
